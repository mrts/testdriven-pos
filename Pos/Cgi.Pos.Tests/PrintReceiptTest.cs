﻿using Cgi.Pos.Core;
using Cgi.Pos.Core.Model;
using Cgi.Pos.Tests.Mocks;
using NUnit.Framework;

namespace Cgi.Pos.Tests
{
    [TestFixture]
    public class PrintReceiptTest
    {
        [Test]
        public void PrintReceiptWithProductsFound()
        {
            // arrange
            var barcode1 = "12345";
            var barcode2 = "23456";
            var barcode3 = "34567";

            var mockFactory = new MockPosControllerDependencyFactory();

            var productRepository = mockFactory.ProductRepository;
            productRepository.SaveOrUpdate(new Product() { Id = barcode1, Price = 12.90m, IsProvincialSalesTaxed = true });
            productRepository.SaveOrUpdate(new Product() { Id = barcode2, Price = 7.15m, IsProvincialSalesTaxed = false });
            productRepository.SaveOrUpdate(new Product() { Id = barcode3, Price = 34.90m, IsProvincialSalesTaxed = true });

            var pos = new PosController(mockFactory);

            // act
            pos.OnStartSale();
            pos.OnBarcode(barcode1);
            pos.OnBarcode(barcode2);
            pos.OnBarcode(barcode3);
            // FIXME: OnFinishSale
            pos.OnPrint();

            var expectedPrintout =
@"12345    12,90 GP
23456     7,15 G
34567    34,90 GP
Subtotal 54,95
GST       2,75
PST       3,82
Total    61,52
";

            // assert
            Assert.That(mockFactory.Printer.Output, Is.EqualTo(expectedPrintout));
        }
    }
}