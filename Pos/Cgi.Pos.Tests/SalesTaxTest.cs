﻿using Cgi.Pos.Core;
using Cgi.Pos.Core.Model;
using Cgi.Pos.Tests.Mocks;
using NUnit.Framework;

namespace Cgi.Pos.Tests
{
    [TestFixture]
    public class SalesTaxTest
    {
        [Test]
        public void SaleWithTaxedProducts()
        {
            // arrange
            var mockFactory = new MockPosControllerDependencyFactory();

            var productRepository = mockFactory.ProductRepository;

            productRepository.SaveOrUpdate(new Product() { Id = "128", Price = 1.00m });
            productRepository.SaveOrUpdate(new Product() { Id = "127", Price = 2.00m, IsProvincialSalesTaxed = true });

            var pos = new PosController(mockFactory);

            // act
            pos.OnStartSale();
            pos.OnBarcode("128");
            pos.OnBarcode("127");
            pos.OnShowSaleTotal();

            // assert
            Assert.That(mockFactory.Display.Output, Is.EqualTo("EUR 3,31"));
        }
    }
}