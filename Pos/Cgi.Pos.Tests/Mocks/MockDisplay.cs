﻿using Cgi.Pos.Core.View;

namespace Cgi.Pos.Tests.Mocks
{
    internal class MockDisplay : IDisplay
    {
        public string Output { get; private set; }

        public void ShowPrice(decimal p)
        {
            Output = string.Format("EUR {0:N2}", p);
        }
    }
}