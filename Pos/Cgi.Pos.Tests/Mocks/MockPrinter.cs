﻿using Cgi.Pos.Core.View;
using System.Text;

namespace Cgi.Pos.Tests.Mocks
{
    internal class MockPrinter : IPrinter
    {
        private const string LINE_FORMAT = "{0,-8}{1,6:0.00}";
        private StringBuilder buffer = new StringBuilder();
        private PrintFormatter formatter = new PrintFormatter();

        public string Output { get { return buffer.ToString(); } }

        public void PrintGoodsAndServicesTax(decimal tax)
        {
            buffer.AppendLine(formatter.FormatGoodsAndServicesTax(tax));
        }

        public void PrintGrandTotal(decimal total)
        {
            buffer.AppendLine(formatter.FormatGrandTotal(total));
        }

        public void PrintProduct(string barcode, decimal price, bool isProvincialTaxed)
        {
            buffer.AppendLine(formatter.FormatProduct(barcode, price, isProvincialTaxed));
        }

        public void PrintProvincialSalesTax(decimal tax)
        {
            buffer.AppendLine(formatter.FormatProvincialSalesTax(tax));
        }

        public void PrintSubtotal(decimal subTotal)
        {
            buffer.AppendLine(formatter.FormatSubTotal(subTotal));
        }
    }
}