﻿using Cgi.Pos.Core;
using Cgi.Pos.Core.Model;
using Cgi.Pos.Core.View;

namespace Cgi.Pos.Tests.Mocks
{
    internal class MockPosControllerDependencyFactory : IPosControllerDependencyFactory
    {
        private readonly MockDisplay display = new MockDisplay();
        private readonly MockPrinter printer = new MockPrinter();
        private readonly MockRepository<Product> productRepository = new MockRepository<Product>();
        private MockRepository<Sale> saleRepository = new MockRepository<Sale>();

        internal MockDisplay Display { get { return display; } }

        internal MockPrinter Printer { get { return printer; } }

        internal MockRepository<Product> ProductRepository { get { return productRepository; } }

        internal MockRepository<Sale> SaleRepository { get { return saleRepository; } }

        public IDisplay CreateDisplay()
        {
            return display;
        }

        public IPrinter CreatePrinter()
        {
            return printer;
        }

        public IRepository<Product> CreateProductRepository()
        {
            return productRepository;
        }

        public IRepository<Sale> CreateSaleRepository()
        {
            return saleRepository;
        }
    }
}