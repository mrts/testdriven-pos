﻿using Cgi.Pos.Core.Model;
using System.Collections.Generic;
using System.Linq;

namespace Cgi.Pos.Tests.Mocks
{
    internal class MockRepository<T> : IRepository<T> where T : IEntity
    {
        private Dictionary<string, T> data = new Dictionary<string, T>();

        public void Delete(T entity)
        {
            data.Remove(entity.Id);
        }

        public T Get(string id)
        {
            var entity = default(T);
            data.TryGetValue(id, out entity);
            return entity;
        }

        public IQueryable<T> Query()
        {
            return data.Values.AsQueryable();
        }

        public void SaveOrUpdate(T entity)
        {
            data[entity.Id] = entity;
        }
    }
}