﻿using Cgi.Pos.Core;
using Cgi.Pos.Core.Model;
using Cgi.Pos.Tests.Mocks;
using NUnit.Framework;

namespace Cgi.Pos.Tests
{
    [TestFixture]
    public class SellOneItemTest
    {
        [Test]
        public void ProductFound()
        {
            // arrange
            var mockFactory = new MockPosControllerDependencyFactory();
            mockFactory.ProductRepository.SaveOrUpdate(new Product() { Id = "123", Price = 1.00m });

            var pos = new PosController(mockFactory);

            // act
            pos.OnBarcode("123");

            // assert
            Assert.That(mockFactory.Display.Output, Is.EqualTo("EUR 1,00"));
        }
    }
}