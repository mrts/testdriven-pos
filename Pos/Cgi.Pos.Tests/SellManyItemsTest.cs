﻿using Cgi.Pos.Core;
using Cgi.Pos.Core.Model;
using Cgi.Pos.Tests.Mocks;
using NUnit.Framework;

namespace Cgi.Pos.Tests
{
    [TestFixture]
    public class SellManyItemsTest
    {
        [Test]
        public void SaleWithManyProductsFound()
        {
            // arrange
            var mockFactory = new MockPosControllerDependencyFactory();

            var productRepository = mockFactory.ProductRepository;
            productRepository.SaveOrUpdate(new Product() { Id = "123", Price = 1.00m });
            productRepository.SaveOrUpdate(new Product() { Id = "125", Price = 2.00m });

            var pos = new PosController(mockFactory);

            // act
            pos.OnStartSale();
            pos.OnBarcode("123");
            pos.OnBarcode("125");
            pos.OnShowSaleTotal();

            // assert
            Assert.That(mockFactory.Display.Output, Is.EqualTo("EUR 3,15"));
        }
    }
}