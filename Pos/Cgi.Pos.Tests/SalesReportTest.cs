﻿using Cgi.Pos.Core;
using Cgi.Pos.Core.Model;
using Cgi.Pos.Core.Report;
using Cgi.Pos.Tests.Mocks;
using Microsoft.QualityTools.Testing.Fakes;
using NUnit.Framework;
using System;

namespace Cgi.Pos.Tests
{
    [TestFixture]
    public class SalesReportTest
    {
        [Test]
        public void SalesReportWithProductsFound()
        {
            // arrange
            var barcode1 = "12345";
            var barcode2 = "23456";
            var barcode3 = "34567";
            var barcode4 = "34567";

            var mockFactory = new MockPosControllerDependencyFactory();

            var productRepository = mockFactory.ProductRepository;
            productRepository.SaveOrUpdate(new Product() { Id = barcode1, Price = 12.90m, IsProvincialSalesTaxed = true });
            productRepository.SaveOrUpdate(new Product() { Id = barcode2, Price = 7.15m, IsProvincialSalesTaxed = false });
            productRepository.SaveOrUpdate(new Product() { Id = barcode3, Price = 34.90m, IsProvincialSalesTaxed = true });
            productRepository.SaveOrUpdate(new Product() { Id = barcode4, Price = 5.85m, IsProvincialSalesTaxed = false });

            var pos = new PosController(mockFactory);

            var salesReport = new SalesReport(mockFactory.SaleRepository);

            var sale1FakeDate = new DateTime(2011, 10, 12, 19, 24, 0);
            var sale2FakeDate = new DateTime(2011, 10, 12, 20, 18, 0);
            var reportFakeDate = new DateTime(2011, 10, 13, 15, 24, 0);

            string reportOutput;

            // act
            using (ShimsContext.Create())
            {
                System.Fakes.ShimDateTime.NowGet = () => sale1FakeDate;

                // sale 1
                pos.OnStartSale();
                pos.OnBarcode(barcode1);
                pos.OnBarcode(barcode2);
                pos.OnBarcode(barcode3);
                pos.OnFinishSale();

                System.Fakes.ShimDateTime.NowGet = () => sale2FakeDate;

                // sale 2
                pos.OnStartSale();
                pos.OnBarcode(barcode2);
                pos.OnBarcode(barcode4);
                pos.OnFinishSale();

                System.Fakes.ShimDateTime.NowGet = () => reportFakeDate;

                reportOutput = salesReport.Output;
            }

            const string TIMESTAMP_FORMAT = "yyyy-MM-dd HH:mm";

            string expectedReportOutput = String.Format(
@"SALES REPORT as at {0}
""Date"", ""Subtotal"", ""GST"", ""PST"", ""Total""
""{1}"", ""25,90"", ""1,30"", ""1,03"", ""28,23""
""{2}"", ""13,00"", ""0,65"", ""0,00"", ""13,65""
""Total"", ""38,90"", ""1,95"", ""1,03"", ""41,88""",
                reportFakeDate.ToString(TIMESTAMP_FORMAT),
                sale1FakeDate.ToString(TIMESTAMP_FORMAT),
                sale2FakeDate.ToString(TIMESTAMP_FORMAT));

            Assert.That(reportOutput, Is.EqualTo(expectedReportOutput));
        }
    }
}