﻿using System.Linq;

namespace Cgi.Pos.Core.Model
{
    public interface IRepository<T> where T : IEntity
    {
        void Delete(T entity);

        T Get(string id);

        IQueryable<T> Query();

        void SaveOrUpdate(T entity);
    }
}