﻿namespace Cgi.Pos.Core.Model
{
    public class Product : IEntity
    {
        public string Id { get; set; }

        public bool IsProvincialSalesTaxed { get; set; }

        public decimal Price { get; set; }
    }
}