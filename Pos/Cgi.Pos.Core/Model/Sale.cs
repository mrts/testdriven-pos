﻿using Cgi.Pos.Core.Calculation;
using System;

namespace Cgi.Pos.Core.Model
{
    public class Sale : IEntity
    {
        private Guid guid;
        private DateTime saleTime;
        private TaxAndTotalCalculator taxCalculator;

        public Sale(TaxAndTotalCalculator taxCalculator)
        {
            guid = Guid.NewGuid();
            saleTime = DateTime.Now;
            this.taxCalculator = taxCalculator;
        }

        public decimal GoodsAndServicesTax { get { return taxCalculator.GoodsAndServicesTax; } }

        public decimal GrandTotal { get { return taxCalculator.GrandTotal; } }

        public string Id
        {
            get { return guid.ToString(); }
            set { throw new NotImplementedException(); }
        }

        public decimal ProvincialSalesTax { get { return taxCalculator.ProvincialSalesTax; } }

        public decimal Subtotal { get { return taxCalculator.SubTotal; } }

        public DateTime Timestamp { get { return saleTime; } }
    }
}