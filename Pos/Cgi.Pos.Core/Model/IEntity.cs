﻿namespace Cgi.Pos.Core.Model
{
    public interface IEntity
    {
        string Id { get; set; }
    }
}