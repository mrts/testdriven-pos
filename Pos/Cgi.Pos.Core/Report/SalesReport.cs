﻿using Cgi.Pos.Core.Model;
using System;
using System.Linq;
using System.Text;

namespace Cgi.Pos.Core.Report
{
    public class SalesReport
    {
        private const string DATE_FORMAT = "yyyy-MM-dd HH:mm";
        private const string LINE_FORMAT = @"""{0}"", ""{1:0.00}"", ""{2:0.00}"", ""{3:0.00}"", ""{4:0.00}""";

        private IRepository<Sale> saleRepository;

        public SalesReport(IRepository<Sale> saleRepository)
        {
            this.saleRepository = saleRepository;
        }

        public string Output
        {
            get
            {
                StringBuilder st = new StringBuilder();

                st.AppendLine(string.Format("SALES REPORT as at {0}", DateTime.Now.ToString(DATE_FORMAT)));
                st.AppendLine(string.Format(LINE_FORMAT, "Date", "Subtotal", "GST", "PST", "Total"));

                var totals = new TotalsAccumulator();

                var query = from sale in saleRepository.Query()
                            orderby sale.Timestamp ascending
                            select sale;

                foreach (var sale in query)
                {
                    totals.Add(sale);
                    AppendLine(st, sale);
                }

                st.AppendFormat(LINE_FORMAT, "Total",
                    totals.SubtotalSum, totals.GoodsAndServicesTaxSum,
                    totals.ProvincialSalesTaxSum, totals.GrandTotalSum);

                return st.ToString();
            }
        }

        private static void AppendLine(StringBuilder st, Sale sale)
        {
            st.AppendLine(string.Format(LINE_FORMAT, sale.Timestamp.ToString(DATE_FORMAT),
                sale.Subtotal, sale.GoodsAndServicesTax,
                sale.ProvincialSalesTax, sale.GrandTotal));
        }

        private struct TotalsAccumulator
        {
            public decimal GoodsAndServicesTaxSum;
            public decimal GrandTotalSum;
            public decimal ProvincialSalesTaxSum;
            public decimal SubtotalSum;

            internal void Add(Sale sale)
            {
                GoodsAndServicesTaxSum += sale.GoodsAndServicesTax;
                GrandTotalSum += sale.GrandTotal;
                ProvincialSalesTaxSum += sale.ProvincialSalesTax;
                SubtotalSum += sale.Subtotal;
            }
        }
    }
}