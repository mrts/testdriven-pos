﻿using Cgi.Pos.Core.Model;
using Cgi.Pos.Core.View;

namespace Cgi.Pos.Core
{
    public interface IPosControllerDependencyFactory
    {
        IDisplay CreateDisplay();

        IPrinter CreatePrinter();

        IRepository<Product> CreateProductRepository();

        IRepository<Sale> CreateSaleRepository();
    }
}