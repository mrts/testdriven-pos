﻿namespace Cgi.Pos.Core.View
{
    public interface IDisplay
    {
        string Output { get; }

        void ShowPrice(decimal p);
    }
}