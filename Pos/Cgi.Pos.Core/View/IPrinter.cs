﻿namespace Cgi.Pos.Core.View
{
    public interface IPrinter
    {
        string Output { get; }

        void PrintGoodsAndServicesTax(decimal tax);

        void PrintGrandTotal(decimal p);

        void PrintProduct(string barcode, decimal price, bool isProvincialTaxed);
        void PrintProvincialSalesTax(decimal tax);

        void PrintSubtotal(decimal total);
    }
}