﻿namespace Cgi.Pos.Core.View
{
    public class PrintFormatter
    {
        private const string LINE_FORMAT = "{0,-8}{1,6:0.00}";

        public string FormatGoodsAndServicesTax(decimal tax)
        {
            return FormatAmount("GST", tax);
        }

        public string FormatGrandTotal(decimal total)
        {
            return FormatAmount("Total", total);
        }

        public string FormatProduct(string barcode, decimal price, bool isProvincialTaxed)
        {
            return string.Format(LINE_FORMAT + " {2}", barcode, price, isProvincialTaxed ? "GP" : "G");
        }

        public string FormatProvincialSalesTax(decimal tax)
        {
            return FormatAmount("PST", tax);
        }

        public string FormatSubTotal(decimal subTotal)
        {
            return FormatAmount("Subtotal", subTotal);
        }

        private string FormatAmount(string label, decimal tax)
        {
            return string.Format(LINE_FORMAT, label, tax);
        }
    }
}