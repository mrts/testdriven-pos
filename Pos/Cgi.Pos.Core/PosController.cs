﻿using Cgi.Pos.Core.Calculation;
using Cgi.Pos.Core.Model;
using Cgi.Pos.Core.View;
using System.Collections.Generic;
using System.Linq;

namespace Cgi.Pos.Core
{
    public class PosController
    {
        #region Dependencies

        private IDisplay display;
        private IPrinter printer;
        private IRepository<Product> productRepository;
        private IRepository<Sale> saleRepository;

        #endregion Dependencies

        private IList<Product> saleItems;

        public PosController(IPosControllerDependencyFactory factory)
        {
            this.display = factory.CreateDisplay();
            this.printer = factory.CreatePrinter();
            this.productRepository = factory.CreateProductRepository();
            this.saleRepository = factory.CreateSaleRepository();

            this.saleItems = new List<Product>();
        }

        public void OnBarcode(string id)
        {
            var product = productRepository.Get(id);
            saleItems.Add(product);
            display.ShowPrice(product.Price);
        }

        public void OnFinishSale()
        {
            saleRepository.SaveOrUpdate(new Sale(GetCalculator()));
        }

        public void OnPrint()
        {
            foreach (var product in saleItems)
                printer.PrintProduct(product.Id, product.Price, product.IsProvincialSalesTaxed);

            var calculator = GetCalculator();

            printer.PrintSubtotal(calculator.SubTotal);
            printer.PrintGoodsAndServicesTax(calculator.GoodsAndServicesTax);
            printer.PrintProvincialSalesTax(calculator.ProvincialSalesTax);
            printer.PrintGrandTotal(calculator.GrandTotal);
        }

        public void OnShowSaleTotal()
        {
            var calculator = GetCalculator();
            display.ShowPrice(calculator.GrandTotal);
        }

        public void OnStartSale()
        {
            this.saleItems = new List<Product>();
        }

        private TaxAndTotalCalculator GetCalculator()
        {
            return new TaxAndTotalCalculator(GetSubTotal(), GetProvincialSalesTaxAmount());
        }

        private decimal GetProvincialSalesTaxAmount()
        {
            return (from saleItem in saleItems
                    where saleItem.IsProvincialSalesTaxed
                    select saleItem.Price).Sum();
        }

        private decimal GetSubTotal()
        {
            return saleItems.Sum(item => item.Price);
        }
    }
}
