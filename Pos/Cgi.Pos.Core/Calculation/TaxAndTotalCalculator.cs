﻿namespace Cgi.Pos.Core.Calculation
{
    public class TaxAndTotalCalculator
    {
        public const decimal GOODS_AND_SERVICES_TAX = 0.05m;
        public const decimal PROVINCIAL_SALES_TAX = 0.08m;

        private decimal provincialSaleTotal;
        private decimal saleTotal;

        public TaxAndTotalCalculator(decimal saleTotal, decimal provincialSaleTotal)
        {
            this.saleTotal = saleTotal;
            this.provincialSaleTotal = provincialSaleTotal;
        }

        public decimal GoodsAndServicesTax
        {
            get { return GetGoodsAndServicesTax(saleTotal); }
        }

        public decimal GrandTotal
        {
            get { return SubTotal + GoodsAndServicesTax + ProvincialSalesTax; }
        }

        public decimal ProvincialSalesTax
        {
            get { return GetProvincialSalesTax(provincialSaleTotal); }
        }

        public decimal SubTotal
        {
            get { return saleTotal; }
        }

        private static decimal GetGoodsAndServicesTax(decimal total)
        {
            return total * GOODS_AND_SERVICES_TAX;
        }

        private static decimal GetProvincialSalesTax(decimal provincialSalesTaxedAmount)
        {
            return provincialSalesTaxedAmount * PROVINCIAL_SALES_TAX;
        }
    }
}